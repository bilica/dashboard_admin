/**
 * Created by mcabral on 2016/11/07
 */

Template.users.onCreated(function () {
});

Template.users.onRendered(function () {

    var instance = this;

    var sub_to_users = instance.subscribe('users');

    instance.autorun(function () {

        if (sub_to_users.ready()) {

        }

    });

});

Template.users.helpers({

    settings: function () {
        return {
            collection: Users,
            rowsPerPage: 40,
            showFilter: true,
            fields: [ {'key': 'username', label: 'Username'},
                {'key': 'profile.nome', label: 'Nome'},
                {'key': 'profile.agency_id', label: 'Agência'},
                {'key': 'emails.0.address', label: 'Email Address'},
                {'key': 'status.lastLogin.date', label: 'Último Acesso'},
                ]
        };
    },
    users: function(){
        return Users.find({});
    }

});

Template.users.events({


});
