Session.setDefault("current_agency_id", null);

Template.MasterLayout.helpers({
    "activeIfTemplateIs": function(template){
        var currentRoute = Router.current();
        if (currentRoute && template == currentRoute.route.getName())
            return 'active';
        else
            return '';
    }
});


Template.MasterLayout.helpers({
});


Template.MasterLayout.onCreated(function () {
});

Template.MasterLayout.onRendered(function () {

    $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 240
            edge: 'left', // Choose the horizontal origin
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        }
    );
    // // Initialize collapse button
    // $(".button-collapse").sideNav();
    // // Initialize collapsible (uncomment the line below if you use the dropdown variation)
    // $('.collapsible').collapsible();
});
