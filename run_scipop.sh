#!/bin/bash
#export DDP_DEFAULT_CONNECTION_URL=http://localhost:3030

export MONGO_URL=mongodb://localhost:27017/scipop
export MONGO_OPLOG_URL=mongodb://localhost:27017/local

export MONGO_LOGS_URL=mongodb://localhost:27017/scipop
export MONGO_OPLOG_LOGS_URL=mongodb://localhost:27017/local

#export NODE_OPTIONS='--debug-brk'

meteor --port 3002 

