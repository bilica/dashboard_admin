Agencies = new Meteor.Collection("agencies");

Agencies.findAgenciesForDomain = function (domain) {
    return Agencies.find({"domains": {$regex: domain}}).fetch();
};