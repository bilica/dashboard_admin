//Iron.utils.debug = true;

Router.configure({
    layoutTemplate: 'MasterLayout'
});

// Router.onBeforeAction(function() {
//     console.log("Meteor.user() ", Meteor.user());
//     if (!Roles.userIsInRole(Meteor.user(), 'admin')) {
//         this.render("dashboard");
//     } else {
//         this.next();
//     }
// }, {only : 'admin' });

Router.route('/', function () {
    this.render('main');
}, {name: "main"});

Router.route('/users', function () {
    this.render('users');
}, {name: "users"});

